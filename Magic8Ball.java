//imports modules we will use
//from file location, and the module/method you want to import
import java.util.Random;
import java.util.Scanner;

// initial main java file by creating a class for it
public class Magic8Ball
{
    // public static main is how you contruct the file.
    //parameter "String args[]" doesnt change in any new file
    public static void main(String args[]) {

        //initialize "Scanner" module as "myObj" variable
        // set myObj equal to a new instance of the "Scanner()" function
        // set the parameter to scan for as "System.in" for your computers input
        Scanner myObj = new Scanner(System.in);

        //Question to ask the user, simple print statement using "System.out.println()"
        System.out.println("What is you're question?");

        // initialize a new String named "question"
        //set question equal to the scanner instance of myObj using the "nextLine()" method
        // which is apart of myObj because Scanner has this method
        // Scanner.nextLine
        //type the above to see that nextLine will auto fill the same for Scanner as it did for myObj
        String question = myObj.nextLine();

        //inititalize a new Random variable named "r"; set it equal to the "Random()" function
        Random r = new Random();

        // initialize a new integer variable named "choice"
        // random object "r" has a method "nextInt()"
        // "nextInt()" can pass a number as a parameter. will return a random number upto your number
        //so choice will equal 1 + random number between 0-14, so 1-15
        int choice = 1 + r.nextInt(15);

        //test to see what choice will equal by removing the "// from the next line"
        //System.out.println("Choice:::::::" + choice);

        //initialize a new String named response; set it equal to an empty String ""
        String response = "";


        //if and else if statements to check if choice equals a certain number
        // if it does response will equal a response for that number
        //checks 1 number at a time until it finds a match then executes code
        if (choice == 1)
            response = "It is certain";
        else if (choice == 2)
            response = "It is decidedly so";
        else if (choice == 3)
            response = "Without a doubt";
        else if (choice == 4)
            response = "Yes - definitely";
        else if (choice == 5)
            response = "You may rely on it";
        else if (choice == 6)
            response = "As I see it, Yes";
        else if (choice == 7)
            response = "Most likely";
        else if (choice == 8)
            response = "Outlook good";
        else if (choice == 9)
            response = "Signs point to yes";
        else if (choice == 10)
            response = "Yes";
        else if (choice == 11)
            response = "Reply hazy, try again";
        else if (choice == 12)
            response = "Ask again later";
        else if (choice == 13)
            response = "Better not tell you now";
        else if (choice == 14)
            response = "Cannot predict now";
        else if (choice == 15)
            response = "Concentrate and ask again";

        //final else means if no match has been found/specified this final code executes
        else
            response = "ERROR ERROR EGGGTERMINATE";

        //print the response and a reiteration of your question you asked.
        // "\n" means next line when inside a string
        // ("Add a string" + " " + "together with plus signs" + ".")
        //is the same as  ("Add a string together with plus signs.")
        System.out.println("\n\nYour question was: \n"+ question + "\n\nMagic 8-ball says::\n" + response);

        //that error was saying i had to close the Scanner instance of "myObj"
        // it ran without it just fine, but good practive
        myObj.close();
    }
}

//remember each curly bracket {} is a block of code
//curly brakcets may also indicate dictionaries {"a": 1, "b": 7} more on this later
// parenthesis () indicate a function, can pass (parameters) into functions sometimes
// brackets [] indicate a list of objectslike [1, 2, 4, "string", 7, "4", {"a":1, "b":2}]
